:- consult('utilitarios').
:- consult('fabricas').
:- consult('cidades').


/* ----- getIt1 ----- */
getIt1(Cam):-
	fabrica(Fabrica,_,_),
	tsp1(Fabrica,Cam,_).
	
/* ----- TSP1: Procura em massa ----- */	
tsp1(C,Cam,R):-	findall(X, cidade(X,_,_), LstC),
				length(LstC, N1),
				N is N1 + 1,
				findall((Dist,Caminho),(dfs_1(C,N,Caminho,Dist)),Lista),
				sort(0,@=<,Lista,[H|_]),
				H = (R,Cam).
						
dfs_1(Orig,N,Cam,Dist):-	dfs2_1(Orig,N,[Orig],Cam1,0,Dist1),
							append([Orig],Cam1,Cam),
							Cam1 =	[H|_],
							dist_cities(Orig,H,Dist2),
							Dist is Dist1 + Dist2.
						
dfs2_1(_,N,LA,LA,Aux,Dist):-		length(LA,N),
									Dist is Aux.						
dfs2_1(Act,N,LA,Cam,Aux,Dist):-		\+ length(LA,N),
									dist_cities(Act, X,CustoX),
									\+ member(X,LA),
									Aux1 is Aux + CustoX,
									dfs2_1(X,N,[X|LA],Cam,Aux1,Dist).

/* ----- getIt ----- */
getIt(Cam):-
	fabrica(Fabrica,_,_),
	tsp3(Fabrica,Cam,_).


/* ----- TSP3: Nearest Neighbour + Cross Elimination ----- */
%tsp3(+Orig,-Cam,-Dist).
%Devolve o itenerario ideal.
tsp3(Orig,Cam,Dist):-
	tsp2(Orig,CamCruzado),
	c2s(CamCruzado,ListaSegmentos),
	remover_cruzamentos(ListaSegmentos, ListaSegmentos, ListaSegmentosFinal,Orig),
	s2c(ListaSegmentosFinal,Cam),
	calc_Dist(Cam,Dist).

/* ----- TSP2: Nearest Neighbour ----- */
%tsp2(+Orig,-Cam).
%Devolve um caminho com as ligações das cidades mais proximas.
tsp2(C,Cam):-
	findall(X, cidade(X,_,_), LstC),
	length(LstC, N),
	dfs(C,N,Cam1,_),
	reverse(Cam1,Cam).
dfs(Orig,N,Cam,Dist):-
	dfs2(Orig,N,[Orig],Cam1,0,Dist1),
	append([Orig],Cam1,Cam),
	Cam1 =	[H|_],
	dist_cities(Orig,H,Dist2),
	Dist is Dist1 + Dist2.

dfs2(Orig,N,LA,Cam,Aux,Dist):-
	length(LA,1),
	findall((Custo,C),(dist_cities(Orig, C,Custo),\+ member(C,LA)),Lista),
	sort(0,@=<,Lista,[H|_]),
	H = (CustoX,X),
	Aux1 is Aux + CustoX,
	dfs2(X,N,[X|LA],Cam,Aux1,Dist),!.
dfs2(_,N,LA,LA,Aux,Dist):-
	length(LA,N),
	Dist is Aux,!.
dfs2(Act,N,LA,Cam,Aux,Dist):-
	\+ length(LA,N),
	findall((Custo,C),(dist_cities(Act, C,Custo),\+ member(C,LA)),Lista),
	sort(0,@=<,Lista,[H|_]),
	H = (CustoX,X),
	Aux1 is Aux + CustoX,
	dfs2(X,N,[X|LA],Cam,Aux1,Dist).

/* ----- remover_cruzamentos ----- */
%remover_cruzamentos(+ListaSegmentos, +ListaSegmentos, -ListaSegmentosFinal,+Orig).
%percorre todos segmentos presentes em ListaSegmentos e compara cada uma com a restante.
remover_cruzamentos([_],ListaSegmentos,ListaSegmentos,_).
remover_cruzamentos([H|T],ListaSegmentos,ListaSegFinal,Orig):-
	remover_cruz(H,T,ListaSegmentos,NovaListaSeg,Orig),
	(NovaListaSeg \= ListaSegmentos,
		remover_cruzamentos(NovaListaSeg,NovaListaSeg,ListaSegFinal,Orig),!);
	remover_cruzamentos(T,ListaSegmentos,ListaSegFinal,Orig).

/* ----- remover_cruz ----- */
%remover_cruz(+Seg,+Rest,+ListaSegmentos,-NovaListaSeg,+Orig).
%verifica se há interseções entre o segmento Seg e os restantes segmentos presentes em Rest.
remover_cruz(_,[],ListaSeg,ListaSeg,_).
remover_cruz(Seg,[H|T],ListaSeg,NovaListaSeg,Orig):-
	Seg = [C1,C2],
	H = [C3,C4],
	(C2 \= C3,C1 \= C4,
	getPonto(C1,P1), getPonto(C2,Q1), getPonto(C3,P2), getPonto(C4,Q2),
	doIntersect(P1,Q1,P2,Q2),
		select(Seg,ListaSeg,ListaSeg1),
		select(H,ListaSeg1,ListaSeg2),
		append(ListaSeg2,[[C1,C3],[C2,C4]],ListaUsar),
		rGraph(Orig,ListaUsar,NovaListaSeg),!);
	remover_cruz(Seg,T,ListaSeg,NovaListaSeg,Orig).

/* ----- c2s ----- */
%c2s(+CityList,-SegList).
%transforma a lista de cidades numa lista de segmentos.
c2s([H|T],R):-
	length([H|T],2),
	T = [H1|_],
	R = [[H,H1]],!.
c2s([H|T],R):-
	c2s(T,R1),
	T = [H1|_],
	R = [[H,H1]|R1].

/* ----- s2c ----- */
%s2c(+SegList,-CityList).
%transforma a lista de segmentos numa lista de cidades.
s2c([[C1,C2]],[C1,C2]):-!.
s2c([H|T],ListaCid):-
	s2c(T,ListaCid1),
	H = [C,_],
	append([C],ListaCid1,ListaCid).

/* ----- getPonto ----- */
%getPonto(+Cidade,-Ponto).
%devolve as coordanadas de uma cidade.
getPonto(Cidade,P):-
	cidade(Cidade,Px,Py),
	geo2linear2(Px,Py,Xp,Yp),
	P = (Xp,Yp).

geo2linear2(Lat,Lon,X,Y):-
	degrees2radians(Lat,LatR),
	degrees2radians(Lon,LonR),
	X is round(6371*cos(LatR)*cos(LonR)),
	Y is round(6371*cos(LatR)*sin(LonR)).

/* ----- calc_Dist ----- */
%calc_Dist(+Caminho,-Distancia).
%devolve a distancia de um caminho
calc_Dist([H|T],Dist):-	length([H|T],2),
						T = [Ht|_],
						dist_cities(H,Ht,Dist),
						!.
calc_Dist([H|T],Dist):-	calc_Dist(T,Dist1),
						T = [Ht|_],
						dist_cities(H,Ht,Dist2),
						Dist is Dist1 + Dist2.

dist_cities(C1,C2,Dist):-
    cidade(C1,Lat1,Lon1),
    cidade(C2,Lat2,Lon2),
    distance(Lat1,Lon1,Lat2,Lon2,Dist).
