:- consult('utilitarios').
:- consult('calcIt').
:- consult('auxiliar').
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).

:- http_handler('/getIt', register_places, []).
:- http_handler('/getFac', getFactory, []).

server(Port):-
	http_server(http_dispatch,[port(Port)]).

register_places(Request) :-
    http_parameters(Request,
             [lista(Locais, [list(string)])]),
    register_local(Locais,2),
	findall(Cidade,cidade(Cidade,_,_),Cidades),
	((length(Cidades,L),L<9, 
		getIt1(Cam)); 
	(getIt(Cam))),!,
	clean,
	format('Content-type: text/plain~n~n'),
    format('~w~n',[Cam]).

getFactory(Request) :-
    http_parameters(Request,
             [lista(Locais, [list(string)])]),
    register_local(Locais,1),
	cidade(Cidade,_,_),
	register_factory(Cidade,Fabrica),
	clean,
	format('Content-type: text/plain~n~n'),
    format('~w~n',[Fabrica]).	