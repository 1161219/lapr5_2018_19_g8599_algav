:- consult('utilitarios').
:- consult('cidades').
:- consult('fabricas').

register_local([H|T],N):-
	split_string(H,",","",L),
	nth0(0,L,Nome),
	nth0(1,L,Lat1),
	nth0(2,L,Long1),
	atom_number(Lat1,Lat),
	atom_number(Long1,Long),
  ((N == 1,
    assert(cidade(Nome,Lat,Long)),
    register_local_aux(T,N));
  (
    assert(fabrica(Nome,Lat,Long)),
    register_local_aux([H|T],N))),!.
register_local_aux([],_):-!.
register_local_aux([H|T],N):-
  split_string(H,",","",L),
  nth0(0,L,Nome),
  nth0(1,L,Lat1),
  nth0(2,L,Long1),
  atom_number(Lat1,Lat),
  atom_number(Long1,Long),
  ((N == 1,
    assert(fabrica(Nome,Lat,Long)));
  (
    assert(cidade(Nome,Lat,Long)))),!,
register_local_aux(T,N).

clean:-
	retractall(cidade(_,_,_)),
	retractall(fabrica(_,_,_)).


/* ----- REGISTER_FACTORY ----- */
register_factory(Cidade,Fab):-
	findall(Fabrica,fabrica(Fabrica,_,_),List),
	tsp2(Cidade,List,Fab).

/* ----- TSP2: Nearest Neighbour ----- */
%tsp2(+Orig,+List,-Fab).
%Devolve a fabrica mais proxima.
tsp2(Orig,List,Fab):-
	tsp2Aux(Orig,List,_,Fab).

tsp2Aux(Orig,[H],Dist,H):-
	dist_fabs(Orig,H,Dist),!.
tsp2Aux(Orig,[H|T],Dist,Fab):-
	tsp2Aux(Orig,T,Dist1,Fab1),
	dist_fabs(Orig,H,Dist2),
	((Dist1 > Dist2,
		Fab = H,Dist is Dist2);
	(Fab = Fab1,Dist is Dist1)),!.

dist_fabs(C,F,Dist):-
    cidade(C,Lat1,Lon1),
    fabrica(F,Lat2,Lon2),
    distance(Lat1,Lon1,Lat2,Lon2,Dist).	