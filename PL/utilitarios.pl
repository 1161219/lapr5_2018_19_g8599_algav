
%Aqui estão contidos variados predicados auxiliares:
	%- distCities(C1, C2, Dist). Medição de distância entre dois pontos, C1 e C2;
	%- doIntersect(P1,Q1,P2,Q2). Deteção de interseções entre dois segmentos formados por P1,Q1 e P2,Q2;
	%- rGraph(Origem, OldList, OrderedList). Reorganização da lista de cidades;





/* ----- Distância entre dois pontos ----- */
degrees2radians(Deg,Rad):-
	Rad is Deg*0.0174532925.

%distance(latitude_first_point,longitude_first_point,latitude_second_point,longitude_second_point,distance in meters)
distance(Lat1, Lon1, Lat2, Lon2, Dis2):-
	degrees2radians(Lat1,Psi1),
	degrees2radians(Lat2,Psi2),
	DifLat is Lat2-Lat1,
	DifLon is Lon2-Lon1,
	degrees2radians(DifLat,DeltaPsi),
	degrees2radians(DifLon,DeltaLambda),
	A is sin(DeltaPsi/2)*sin(DeltaPsi/2)+ cos(Psi1)*cos(Psi2)*sin(DeltaLambda/2)*sin(DeltaLambda/2),
	C is 2*atan2(sqrt(A),sqrt(1-A)),
	Dis1 is 6371000*C,
	Dis2 is round(Dis1).

/* ----- Deteção de cruzamentos entre 2 segmentos ----- */
doIntersect(P1,Q1,P2,Q2):-
	orientation4cases(P1,Q1,P2,Q2,O1,O2,O3,O4),
	(
    % General case
    O1 \== O2 , O3 \== O4,!;
    O1 == 0, onSegment(P1, P2, Q1),!; % p1, q1 and p2 are colinear and p2 lies on segment p1q1
    O2 == 0, onSegment(P1, Q2, Q1),!; % p1, q1 and p2 are colinear and q2 lies on segment p1q1
    O3 == 0, onSegment(P2, P1, Q2),!; % p2, q2 and p1 are colinear and p1 lies on segment p2q2
    O4 == 0, onSegment(P2, Q1, Q2),!  % p2, q2 and q1 are colinear and q1 lies on segment p2q2
  ).


/* 3 Pontos Colineares, verifica se R se posiciona entre P e Q */
/* onSegment(P, Q, R). */
onSegment((PX,PY), (QX,QY), (RX,RY)):-
    QX =< max(PX,RX),
    QX >= min(PX,RX),
    QY =< max(PY,RY),
    QY >= min(PY,RY).

/*
To find orientation of ordered triplet (p, q, r).
The function returns following values
0 --> p, q and r are colinear
1 --> Clockwise
2 --> Counterclockwise
*/
orientation((PX,PY), (QX,QY), (RX,RY), Orientation):-
	Val is (QY - PY) * (RX - QX) - (QX - PX) * (RY - QY),
	(
		Val == 0, !, Orientation is 0;
		Val >0, !, Orientation is 1;
		Orientation is 2
	).

orientation4cases(P1,Q1,P2,Q2,O1,O2,O3,O4):-
    orientation(P1, Q1, P2,O1),
    orientation(P1, Q1, Q2,O2),
    orientation(P2, Q2, P1,O3),
    orientation(P2, Q2, Q1,O4).







/* ----- Reorganização do Grafo ----- */
% Exemplo: rGraph(a,[[a,b],[b,c],[c,d],[e,f],[d,f],[e,a]],R).
rGraph(Orig,[[Orig,Z]|R],R2):-!,
	reorderGraph([[Orig,Z]|R],R2).
rGraph(Orig,R,R3):-
	member([Orig,X],R),!,
	delete(R,[Orig,X],R2),
	reorderGraph([[Orig,X]|R2],R3).
rGraph(Orig,R,R3):-
	member([X,Orig],R),
	delete(R,[X,Orig],R2),
	reorderGraph([[Orig,X]|R2],R3).


reorderGraph([],[]).

reorderGraph([[X,Y],[Y,Z]|R],[[X,Y]|R1]):-
	reorderGraph([[Y,Z]|R],R1).

reorderGraph([[X,Y],[Z,W]|R],[[X,Y]|R2]):-
	Y\=Z,
	reorderGraph2(Y,[[Z,W]|R],R2).

reorderGraph2(_,[],[]).
reorderGraph2(Y,R1,[[Y,Z]|R2]):-
	member([Y,Z],R1),!,
	delete(R1,[Y,Z],R11),
	reorderGraph2(Z,R11,R2).
reorderGraph2(Y,R1,[[Y,Z]|R2]):-
	member([Z,Y],R1),
	delete(R1,[Z,Y],R11),
	reorderGraph2(Z,R11,R2).



/* ----- Outros ----- */
apaga(X,[X|T],T):-!.
apaga(X,[H|T],[H|L]):-apaga(X,T,L).
/* ------------------ */
